import pandas as pd
import re
import requests
from clint.textui import progress

# load csv to dataframe
df = pd.read_csv('springerbooks.csv', delimiter=';')

# cleanup
df.columns = df.columns.str.strip().str.lower().str.replace(' ', '_').str.replace('(', '').str.replace(')', '')

# create new dataframe out of subset
print("\n\n***Download files from Springer*** \n\n")
searchstring = input('What word should the book title include? \n')
book_subset = df[df['book_title'].str.contains(searchstring, flags=re.IGNORECASE, regex=True)]

def pdf_url_list(dataframe):
    """Takes dataframe with DOI URLs and returns a list of URLS with PDF versions of book"""
    df_doi_url = dataframe['doi_url']
    url_list = []
    for row in df_doi_url:
        temp_url = row.replace('http://doi.org', 'https://link.springer.com/content/pdf/')
        pdf_url = temp_url + '.pdf'
        url_list.append(pdf_url)
    return url_list

url_list = pdf_url_list(book_subset)

print("Found {} books with ".format(len(url_list)) + "'" + searchstring + "' " + "in the title.")

for item in url_list:
	r = requests.get(item, stream=True)
	try:
		d = r.headers['content-disposition']
		header_filename = d.split('filename=')
		filename = header_filename[1]
	except KeyError:
		print(item)

	with open(filename, 'wb') as sav:
		total_length = int(r.headers.get('content-length'))
		for chunk in progress.bar(r.iter_content(chunk_size=1024), expected_size=(total_length/1024) + 1):
			sav.write(chunk)